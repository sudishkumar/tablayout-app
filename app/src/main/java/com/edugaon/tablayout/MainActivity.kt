package com.edugaon.tablayout

import android.icu.text.CaseMap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tabLayout = findViewById<TabLayout>(R.id.myTabLayout)
        val myViewPager = findViewById<ViewPager>(R.id.myViewpager)
        val myAdapter = ViewPagerAdapter(supportFragmentManager)

        myAdapter.addFragment(TeachersFragment(), "Teachers")
        myAdapter.addFragment(StudentsFragment(), "Students")

        myViewPager.adapter  = myAdapter
        tabLayout.setupWithViewPager(myViewPager)
    }

    class ViewPagerAdapter(fragment: FragmentManager) : FragmentPagerAdapter(fragment){
        private var fragmentList: MutableList<Fragment> = ArrayList()
        private var titleList : MutableList<String> = ArrayList()

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        fun addFragment(fragment: Fragment, title: String){
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }
}