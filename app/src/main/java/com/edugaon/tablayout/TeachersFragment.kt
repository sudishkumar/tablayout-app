package com.edugaon.tablayout

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class TeachersFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rowView = inflater.inflate(R.layout.fragment_teachers, container, false)
        val btnViewPager = rowView.findViewById<Button>(R.id.button_forViewPager)
        btnViewPager.setOnClickListener {
            val intent = Intent(activity, ViewPagerActivity::class.java)
            startActivity(intent)
        }
        return rowView
    }
}